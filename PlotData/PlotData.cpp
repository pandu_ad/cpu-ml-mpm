#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <string>
#include <algorithm> 

using namespace std;

size_t split(const std::string &txt, std::vector<std::string> &strs, char ch)
{
	size_t pos = txt.find(ch);
	size_t initialPos = 0;
	strs.clear();

	// Decompose statement
	while (pos != std::string::npos) {
		strs.push_back(txt.substr(initialPos, pos - initialPos));
		initialPos = pos + 1;

		pos = txt.find(ch, initialPos);
	}

	// Add the last one
	strs.push_back(txt.substr(initialPos, std::min(pos, txt.size()) - initialPos + 1));

	return strs.size();
}

int main() {
	int line = 0;
	int index = 0;

	string STRING;
	ifstream myReadFile;
	myReadFile.open("../MPM/DataY.txt");
	char output[100];
	getline(myReadFile, STRING); // Saves the line in STRING.
	while (line < 1000 && !myReadFile.eof()) // To get you all the lines.
	{
		getline(myReadFile, STRING); // Saves the line in STRING.
		if (line % 2 == 1) {
			cout << STRING + "\n\n"; // Prints our STRING.
		}
		else {
			std::vector<std::string> v;

			string arr[3][3];
			split(STRING, v, ' ');

			string data;
			for (int idx = 0; idx < 27; idx++) {
				int dataElmIdx = idx % 3;
				int dataIdx = idx / 3;

				data += " " + v[idx];
				if (dataElmIdx == 2) {
					int i = dataIdx / 3;
					int j = dataIdx % 3;

					arr[j][i] = data;
					data = "";
				}
			}

			string s;
			for (int i = 0; i < 3; i++) {
				for (int j = 0; j < 3; j++) {
					s += arr[i][j] + " | ";
				}
				s += +"\n";
				cout << s;
				s = "";
			}
		}

		line++;
	}
	myReadFile.close();
	system("pause");
	return 0;
}
