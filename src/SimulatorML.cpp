//
// Parallelized by Dody Dharma with Standard STL Thread Library, May 2017
//  Original algorithm implemented by Grant Kot on 3/29/12.
//  Copyright (c) 2012 Grant Kot. All rights reserved.
//
#define numMaterials 4
#include <vector>
#include <math.h>
//#import <dispatch/dispatch.h>
#include <cstdlib>
#include "cinder/app/App.h"
#include <fstream>
#include "floatfann.h"
#include "fann.h"

using namespace std;
using namespace cinder;

struct Material {
	float mass, restDensity, stiffness, bulkViscosity, surfaceTension, kElastic, maxDeformation, meltRate, viscosity, damping, friction, stickiness, smoothing, gravity;
	int materialIndex;

	Material() : mass(1), restDensity(2), stiffness(1), bulkViscosity(1), surfaceTension(0), kElastic(0), maxDeformation(0), meltRate(0), viscosity(.02), damping(.001), friction(0), stickiness(0), smoothing(.02), gravity(.03) {};
};

struct Particle {
	vec3		pos;
	vec3        trail;
	ColorA		color;

	Material* mat;
	float x, y, u, v, gu, gv, T00, T01, T11;
	int cx, cy, gi;
	float px[3];
	float py[3];
	float gx[3];
	float gy[3];

	Particle(Material* mat) : pos(0, 0, 0), color(.1, .5, 1, 1), mat(mat), x(0), y(0), u(0), v(0), T00(0), T01(0), T11(0), cx(0), cy(0), gi(0) {
		memset(px, 0, 12 * sizeof(float));
	}

	Particle(Material* mat, float x, float y) : pos(x, y, 0), color(.1, .5, 1, 1), mat(mat), x(x), y(y), u(0), v(0), T00(0), T01(0), T11(0), cx(0), cy(0), gi(0) {
		memset(px, 0, 12 * sizeof(float));
	}

	Particle(Material* mat, float x, float y, ColorA c) : pos(x, y, 0), color(c), mat(mat), x(x), y(y), u(0), v(0), T00(0), T01(0), T11(0), cx(0), cy(0), gi(0) {
		memset(px, 0, 12 * sizeof(float));
	}


	Particle(Material* mat, float x, float y, float u, float v) :pos(x, y, 0), color(.1, .5, 1, 1), mat(mat), x(x), y(y), u(u), v(v), T00(0), T01(0), T11(0), cx(0), cy(0), gi(0) {
		memset(px, 0, 12 * sizeof(float));
	}

	void initializeWeights(int gSizeY) {
		cx = (int)(x - .5f);
		cy = (int)(y - .5f);
		gi = cx * gSizeY + cy;

		float cx_x = cx - x;
		float cy_y = cy - y;

		// Quadratic interpolation kernel weights - Not meant to be changed
		px[0] = .5f * cx_x * cx_x + 1.5f * cx_x + 1.125f;
		gx[0] = cx_x + 1.5f;
		cx_x++;
		px[1] = -cx_x * cx_x + .75f;
		gx[1] = -2 * cx_x;
		cx_x++;
		px[2] = .5f * cx_x * cx_x - 1.5f * cx_x + 1.125f;
		gx[2] = cx_x - 1.5f;

		py[0] = .5f * cy_y * cy_y + 1.5f * cy_y + 1.125f;
		gy[0] = cy_y + 1.5f;
		cy_y++;
		py[1] = -cy_y * cy_y + .75f;
		gy[1] = -2 * cy_y;
		cy_y++;
		py[2] = .5f * cy_y * cy_y - 1.5f * cy_y + 1.125f;
		gy[2] = cy_y - 1.5f;
	}
};

struct Node {
	int index;
	float mass, particleDensity, gx, gy, u, v, u2, v2, ax, ay, fx, fy;
	float cgx[numMaterials];
	float cgy[numMaterials];
	bool active;
	Node() : mass(0), particleDensity(0), gx(0), gy(0), u(0), v(0), u2(0), v2(0), ax(0), ay(0), fx(0), fy(0), active(false) {
		memset(cgx, 0, 2 * numMaterials * sizeof(float));
	}
	Node(int idx) : index(idx), mass(0), particleDensity(0), gx(0), gy(0), u(0), v(0), u2(0), v2(0), ax(0), ay(0), fx(0), fy(0), active(false) {
		memset(cgx, 0, 2 * numMaterials * sizeof(float));
	}
};

class SimulatorML {
	int gSizeX, gSizeY, gSizeY_3;
	Node* grid;
	vector<Node*> active;
	Material materials[numMaterials];
	float uscip(float p00, float x00, float y00, float p01, float x01, float y01, float p10, float x10, float y10, float p11, float x11, float y11, float u, float v)
	{
		float dx = x00 - x01;
		float dy = y00 - y10;
		float a = p01 - p00;
		float b = p11 - p10 - a;
		float c = p10 - p00;
		float d = y11 - y01;
		return ((((d - 2 * b - dy) * u - 2 * a + y00 + y01) * v +
			((3 * b + 2 * dy - d) * u + 3 * a - 2 * y00 - y01)) * v +
			((((2 * c - x00 - x10) * u + (3 * b + 2 * dx + x10 - x11)) * u - b - dy - dx) * u + y00)) * v +
			(((x11 - 2 * (p11 - p01 + c) + x10 + x00 + x01) * u +
			(3 * c - 2 * x00 - x10)) * u +
				x00) * u + p00;
	}
public:
	vector<Particle> particles;
	float scale;
	SimulatorML() :scale(1.0f) {
		//default
		materials[0].materialIndex = 0;
		materials[0].mass = 1.0f;
		materials[0].viscosity = 0.04f;

		materials[1].materialIndex = 1;
		materials[1].mass = 1.0f;
		materials[1].restDensity = 10.0f;
		materials[1].viscosity = 1.0f;
		materials[1].bulkViscosity = 3.0f;
		materials[1].stiffness = 1.0f;
		materials[1].meltRate = 1.0f;
		materials[1].kElastic = 1.0f;



		materials[2].materialIndex = 2;
		materials[2].mass = 0.7f;
		materials[2].viscosity = 0.03f;


		materials[3].materialIndex = 3;
	}
	void initializeGrid(int sizeX, int sizeY) {
		gSizeX = sizeX;
		gSizeY = sizeY;
		gSizeY_3 = sizeY - 3;
		grid = new Node[gSizeX*gSizeY];
		for (int i = 0; i < gSizeX*gSizeY; i++) {
			grid[i] = Node(i);
		}
	}
	void addParticles() {
		// Material 1
		/*for (int i = 0; i < 100; i++) {
			for (int j = 0; j < 100; j++) {
				Particle p(&materials[0], i*.4 + 6, j*.8 / 5 + 6, ColorA(1, 0.5, 0.5, 1));
				p.initializeWeights(gSizeY);
				particles.push_back(p);
			}
		}*/
		// Material 1
		for (int i = 0; i < 100; i++) {
			for (int j = 0; j < 100; j++) {
				Particle p(&materials[0], i * 2 + 100, j * 2 / 5 + 130, ColorA(1, 0.5, 0.5, 1));
				p.initializeWeights(gSizeY);
				particles.push_back(p);
			}
		}
		//// Material 2
		//for (int i = 0; i < 1; i++) {
		//	for (int j = 0; j < 1; j++) {
		//		Particle p(&materials[1], i*.4 + 150, j*.8 / 5 + 15, ColorA(1, 1, 1, 1));
		//		p.initializeWeights(gSizeY);
		//		particles.push_back(p);
		//	}
		//}

		//// Material 2
		//for (int i = 0; i < 100; i++) {
		//	for (int j = 0; j < 100; j++) {
		//		Particle p(&materials[2], i*.4 + 350, j*.8 / 5 + 15, ColorA(0.5, 1.0, 0.0, 1));
		//		p.initializeWeights(gSizeY);
		//		particles.push_back(p);
		//	}
		//}


	}
	void update(fann *annX, fann *annY) {
		string data;

		const size_t nthreads = thread::hardware_concurrency();
		vector<thread> threads(nthreads);
		mutex critical;

		int nParticles = particles.size();

		std::clock_t start;
		double duration1;
		double duration2;
		double duration3;
		double duration4;
		double duration5;
		double duration6;

		start = std::clock();
		//Parallel Particle Phase 1 , We Split the loop task to the number of thread / CPU core available.
		//Calculate particle mass, velocity and density gradient to grid
		for (int t = 0; t < nthreads; t++)
		{
			threads[t] = thread(bind(
				[&](const int bi, const int ei, const int t)
			{
				// loop over all items
				// for (int pi = 0; pi < nParticles; pi++)
				for (int pi = bi; pi < ei; pi++)
				{
					//Begin Loop 1
					{
						Particle &p = particles[pi];
						Material& mat = *p.mat;

						// input: px py gx gy n
						// Output: gu gv dudx dudy dvdx dvdy
						float fx = 0, fy = 0, gu = 0, gv = 0, dudx = 0, dudy = 0, dvdx = 0, dvdy = 0;
						Node* n = &grid[p.gi];
						float *ppx = p.px;
						float *ppy = p.py;
						float* pgx = p.gx;
						float* pgy = p.gy;
						for (int i = 0; i < 3; i++, n += gSizeY_3) {
							float pxi = ppx[i];
							float gxi = pgx[i];

							for (int j = 0; j < 3; j++, n++) {
								float pyj = ppy[j];
								float gyj = pgy[j];

								float phi = pxi * pyj;
								gu += phi * n->u2;
								gv += phi * n->v2;
							}
						}

						p.x += gu;
						p.y += gv;

						p.gu = gu;
						p.gv = gv;

						p.u += mat.smoothing*(gu - p.u);
						p.v += mat.smoothing*(gv - p.v);

						// Hard boundary correction (Random numbers keep it from clustering)
						if (p.x < 1) {
							p.x = 1 + .01*rand() / RAND_MAX;
						}
						else if (p.x > gSizeX - 2) {
							p.x = gSizeX - 2 - .01*rand() / RAND_MAX;
						}
						if (p.y < 1) {
							p.y = 1 + .01*rand() / RAND_MAX;
						}
						else if (p.y > gSizeY - 2) {
							p.y = gSizeY - 2 - .01*rand() / RAND_MAX;
						}

						// Wall force
						if (p.x < 4) {
							fx += (4 - p.x);
						}
						else if (p.x > gSizeX - 5) {
							fx += (gSizeX - 5 - p.x);
						}
						if (p.y < 4) {
							fy += (4 - p.y);
						}
						else if (p.y > gSizeY - 5) {
							fy += (gSizeY - 5 - p.y);
						}

						// input: p.x p.y
						// Output: p.gi p.cx p.cy
						// Update grid cell index and kernel weights
						int cx = p.cx = (int)(p.x - .5f);
						int cy = p.cy = (int)(p.y - .5f);
						p.gi = cx * gSizeY + cy;

						float x = cx - p.x;
						float y = cy - p.y;

						// Quadratic interpolation kernel weights - Not meant to be changed
						ppx[0] = .5f * x * x + 1.5f * x + 1.125f;
						pgx[0] = x + 1.5f;
						x++;
						ppx[1] = -x * x + .75f;
						pgx[1] = -2 * x;
						x++;
						ppx[2] = .5f * x * x - 1.5f * x + 1.125f;
						pgx[2] = x - 1.5f;

						ppy[0] = .5f * y * y + 1.5f * y + 1.125f;
						pgy[0] = y + 1.5f;
						y++;
						ppy[1] = -y * y + .75f;
						pgy[1] = -2 * y;
						y++;
						ppy[2] = .5f * y * y - 1.5f * y + 1.125f;
						pgy[2] = y - 1.5f;

						// input: p.mat p.u p.v p.gi
						// Output: n->mass n->particleDensity n->u n->v n->cgx n->cgy n->active
						float m = p.mat->mass;
						float mu = m * p.u;
						float mv = m * p.v;
						int mi = p.mat->materialIndex;
						float *px = p.px;
						float *gx = p.gx;
						float *py = p.py;
						float *gy = p.gy;
						n = &grid[p.gi];
						for (int i = 0; i < 3; i++, n += gSizeY_3) {
							float pxi = px[i];
							float gxi = gx[i];

							for (int j = 0; j < 3; j++, n++) {
								float pyj = py[j];
								float gyj = gy[j];

								float phi = pxi * pyj;

								n->fx += fx * phi;
								n->fy += fy * phi;

								// Add particle mass, velocity and density gradient to grid
								n->mass += phi * m;
								n->particleDensity += phi;
								n->u += phi * mu;
								n->v += phi * mv;
								n->gx += gxi * pyj;
								n->gy += pxi * gyj;
								n->active = true;
							}
						}

						p.pos.x = p.x*scale;
						p.pos.y = p.y*scale;
						p.trail.x = (p.x - p.gu)*scale;
						p.trail.y = (p.y - p.gv)*scale;
					}
					//End of Loop 1
				}
			}, t*nParticles / nthreads, (t + 1) == nthreads ? nParticles : (t + 1)*nParticles / nthreads, t));
		}
		for_each(threads.begin(), threads.end(), [](thread& x) {x.join(); });
		// Join The Thread, since there are data dependency for next phase
		duration1 = (std::clock() - start) / (double)CLOCKS_PER_SEC;

		start = std::clock();
		// Add active nodes to list
		active.clear();
		Node* gi = grid;
		int gSizeXY = gSizeX * gSizeY;

		for (int i = 0; i < gSizeXY; i++) {
			Node& n = *(gi);
			if (n.active && n.mass > 0) {
				active.push_back(gi);
				n.active = false;
				n.u2 = 0;
				n.v2 = 0;
			}
			gi++;
		}

		int nActive = active.size();
		duration2 = (std::clock() - start) / (double)CLOCKS_PER_SEC;

		// Update acceleration of nodes
		for (int t = 0; t < nthreads; t++)
		{
			threads[t] = thread(bind(
				[&](const int bi, const int ei, const int t)
			{
				fann *annXtemp = fann_create_from_file("../MLTraining/MPMX1 - w.1.0.3.net");
				fann *annYtemp = fann_create_from_file("../MLTraining/MPMY.net");

				//for (int i = 0; i < nActive; i++) {}
				for (int i = bi; i < ei; i++) {
					Node& n = *active[i];
					int idxX = n.index / gSizeY;
					int idxY = n.index % gSizeY;

					int inputIdx = 0;
					float inputX[75];
					float inputY[75];
					for (int i = -2; i <= 2; i++) {
						for (int j = -2; j <= 2; j++) {
							if (idxX + i < 0 || idxX + i >= gSizeX ||
								idxY + j < 0 || idxY + j >= gSizeY) {
								inputX[inputIdx] = 0;
								inputY[inputIdx] = 0;
								inputIdx++;

								inputX[inputIdx] = 0;
								inputY[inputIdx] = 0;
								inputIdx++;

								inputX[inputIdx] = 0;
								inputY[inputIdx] = 0;
								inputIdx++;
								/*input[inputIdx] = 0;
								inputIdx++;
								input[inputIdx] = 0;
								inputIdx++;*/
							}
							else {
								Node* nTemp = &grid[(idxX + i) * gSizeY + (idxY + j)];
								inputX[inputIdx] = nTemp->particleDensity > 4 ? 4 : nTemp->particleDensity;
								inputY[inputIdx] = nTemp->particleDensity > 4 ? 4 : nTemp->particleDensity;
								inputIdx++;;

								inputX[inputIdx] = nTemp->gx > 2 ? 2 : nTemp->gx < -2 ? -2 : nTemp->gx;
								inputY[inputIdx] = nTemp->gy > 2 ? 2 : nTemp->gy < -2 ? -2 : nTemp->gy;
								inputIdx++;

								inputX[inputIdx] = nTemp->mass == 0 ? 0 : nTemp->u / nTemp->mass;
								inputY[inputIdx] = nTemp->mass == 0 ? 0 : nTemp->v / nTemp->mass;
								inputIdx++;
							}
						}
					}

					fann_type *calc_outX;
					fann_reset_MSE(annXtemp);
					calc_outX = fann_run(annXtemp, inputX);
					float ax = calc_outX[0];

					fann_type *calc_outY;
					fann_reset_MSE(annYtemp);
					calc_outY = fann_run(annYtemp, inputY);
					float ay = calc_outY[0];

					//ax = (calc_outX[0] / n.mass > 20) ? 20 : ax;
					//ax = (calc_outX[0] / n.mass < -20) ? -20 : ax;
					//ay = (calc_outY[0] / n.mass > 20) ? 20 : ay;
					//ay = (calc_outY[0] / n.mass < -20) ? -20 : ay;

					n.ax = (ax + n.fx / n.mass);
					n.ay = (ay + n.fy / n.mass);
				}

			}, t*nActive / nthreads, (t + 1) == nthreads ? nActive : (t + 1)*nActive / nthreads, t));
		}
		for_each(threads.begin(), threads.end(), [](thread& x) {x.join(); });

		start = std::clock();
		// Update particle velocities
		// Add particle velocities back to the grid
		for (int t = 0; t < nthreads; t++)
		{
			threads[t] = thread(bind(
				[&](const int bi, const int ei, const int t)
			{
				//for (int pi = 0; pi < nParticles; pi++){}
				for (int pi = bi; pi < ei; pi++) {
					Particle& p = particles[pi];
					Material& mat = *p.mat;

					// Update particle velocities
					Node* n = &grid[p.gi];
					float *px = p.px;
					float *py = p.py;

					for (int i = 0; i < 3; i++, n += gSizeY_3) {
						float pxi = px[i];

						for (int j = 0; j < 3; j++, n++) {
							float pyj = py[j];

							float phi = pxi * pyj;

							p.u += phi * n->ax;
							p.v += phi * n->ay;
						}
					}

					p.v += mat.gravity;
					p.u *= 1 - mat.damping;
					p.v *= 1 - mat.damping;

					float m = p.mat->mass;
					float mu = m * p.u;
					float mv = m * p.v;

					// Add particle velocities back to the grid
					n = &grid[p.gi];
					for (int i = 0; i < 3; i++, n += gSizeY_3) {
						float pxi = px[i];

						for (int j = 0; j < 3; j++, n++) {
							float pyj = py[j];

							float phi = pxi * pyj;

							n->u2 += phi * mu;
							n->v2 += phi * mv;
						}
					}
				}
			}, t*nParticles / nthreads, (t + 1) == nthreads ? nParticles : (t + 1)*nParticles / nthreads, t));
		}
		for_each(threads.begin(), threads.end(), [](thread& x) {x.join(); });
		duration5 = (std::clock() - start) / (double)CLOCKS_PER_SEC;

		start = std::clock();
		// Update node velocities
		for (int t = 0; t < nthreads; t++)
		{
			threads[t] = thread(bind(
				[&](const int bi, const int ei, const int t)
			{
				//for (int i = 0; i < nActive; i++) {}
				for (int i = bi; i < ei; i++) {
					Node& n = *active[i];
					n.u2 /= n.mass;
					n.v2 /= n.mass;

					n.ax = n.ay = 0;
					n.gx = n.gy = 0;
					n.fx = n.fy = 0;
					n.mass = 0;
					n.particleDensity = 0;
					n.u = 0;
					n.v = 0;
					memset(n.cgx, 0, 2 * numMaterials * sizeof(float));
				}

			}, t*nActive / nthreads, (t + 1) == nthreads ? nActive : (t + 1)*nActive / nthreads, t));
		}
		for_each(threads.begin(), threads.end(), [](thread& x) {x.join(); });
		duration6 = (std::clock() - start) / (double)CLOCKS_PER_SEC;
	}
};
