//
//  Rendering with Vertex Buffer Object, by Dody Dharma on May 2016
//

#include "cinder/app/App.h"
#include "cinder/app/RendererGl.h"
#include "cinder/gl/gl.h"
#include "cinder/Rand.h"
#include "cinder/gl/gl.h"
#include "fann.h"

#include "SimulatorML.cpp"
#include <vector>

using namespace ci;
using namespace ci::app;
using namespace std;

using namespace ci;
using namespace ci::app;
using namespace std;

class FluidMultiThreadCPUApp : public App {
	SimulatorML sML;
	int n;
	GLfloat*            vertices;
	ColorA*             colors;

	struct fann *annX;
	struct fann *annY;

	// Buffer holding raw particle data on GPU, written to every update().
	gl::VboRef			mParticleVbo;
	// Batch for rendering particles with  shader.
	gl::BatchRef		mParticleBatch;
	gl::GlslProgRef     mGlsl;

public:
	void setup() override;
	void mouseDown(MouseEvent event) override;
	void update() override;
	void draw() override;
	void prepareSettings(Settings *settings);
};

void FluidMultiThreadCPUApp::setup()
{
	annX = fann_create_from_file("../MLTraining/MPMX1 - w.1.0.3.net");
	annY = fann_create_from_file("../MLTraining/MPMY1.net");

	sML.initializeGrid(400, 200);
	sML.addParticles();
	sML.scale = 3.0f;
	n = sML.particles.size();
	printf("jumlah partikel %d", n);

	mParticleVbo = gl::Vbo::create(GL_ARRAY_BUFFER, sML.particles, GL_STREAM_DRAW);
	// Describe particle semantics for GPU.
	geom::BufferLayout particleLayout;
	particleLayout.append(geom::Attrib::POSITION, 3, sizeof(Particle), offsetof(Particle, pos));
	particleLayout.append(geom::Attrib::CUSTOM_9, 3, sizeof(Particle), offsetof(Particle, trail));
	particleLayout.append(geom::Attrib::COLOR, 4, sizeof(Particle), offsetof(Particle, color));

	// Create mesh by pairing our particle layout with our particle Vbo.
	// A VboMesh is an array of layout + vbo pairs
	auto mesh = gl::VboMesh::create(sML.particles.size(), GL_POINTS, { { particleLayout, mParticleVbo } });

#if ! defined( CINDER_GL_ES )
	// setup shader
	try {
		mGlsl = gl::GlslProg::create(gl::GlslProg::Format()
			.vertex(loadAsset("vertex.vert"))
			.geometry(loadAsset("geometry.geom"))
			.fragment(loadAsset("fragment.frag")));
	}
	catch (gl::GlslProgCompileExc ex) {
		cout << ex.what() << endl;
		quit();
	}

	gl::Batch::AttributeMapping mapping({ { geom::Attrib::CUSTOM_9, "trailPosition" } });
	mParticleBatch = gl::Batch::create(mesh, mGlsl, mapping);
	gl::pointSize(4.0f);

#else
	mParticleBatch = gl::Batch::create(mesh, gl::GlslProg::create(loadAsset("draw_es3.vert"), loadAsset("draw_es3.frag")), mapping);
#endif

}

void FluidMultiThreadCPUApp::mouseDown(MouseEvent event)
{
}

void FluidMultiThreadCPUApp::update()
{
	sML.update(annX, annY);

	// Copy particle data onto the GPU.
	// Map the GPU memory and write over it.
	void *gpuMem = mParticleVbo->mapReplace();
	memcpy(gpuMem, sML.particles.data(), sML.particles.size() * sizeof(Particle));
	mParticleVbo->unmap();
}

void FluidMultiThreadCPUApp::draw()
{
	// clear out the window with black
	gl::clear(Color(0, 0, 0));
	gl::setMatricesWindowPersp(getWindowSize());
	gl::enableDepthRead();
	gl::enableDepthWrite();

	mParticleBatch->draw();
}


CINDER_APP(FluidMultiThreadCPUApp, RendererGl, [](App::Settings *settings) {
	settings->setWindowSize(1200, 600);
	settings->setMultiTouchEnabled(false);
})
